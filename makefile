.PHONY=clean

MPIPATH=/opt/ebsofts/OpenMPI/4.1.5-GCC-12.3.0

MPIINCLUDES=$(MPIPATH)/include
MPILIBS=$(MPIPATH)/lib64

CC=nvcc

reproducer: reproducer.cu
	$(CC) --std=c++17 -I$(MPIINCLUDES) -L$(MPILIBS) -lmpi $^ -o $@

clean:
	rm -rvf ./*.o reproducer